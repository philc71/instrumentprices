# Solution

Here's a screenshot of the working application
![screenshot](sceenshot.png)

## Server

The server is a ASP.NET core web api using .NET 6, it has the following features

- Uses a repository pattern for an in-memory store of the instruments
- Offers an API to retrieve the initial instruments using the repository
- Launches a market data service in the background using TPL
- Then it uses SignalR to stream price updates

![swagger](swagger.png)

### Improvements (Server)

- Could separate the MarketDataService into its own project and deploy it separately
- Could use something like Polly for resilience and rate limiting
- Use structured logging for debugging purposes
- Use metrics like Prometheus to monitoring health
- Include heartbeats in the market data service
- Use SignalR Groups instead of Clients.All (better targeting when other users may exist in the API e.g. non market data users)
- Has no authentication so may want to lock this down
- Testing

## UI

As seen in the first screenshot, this is built using CRA and has the following features

- Uses Axios and SignalR to communicate with the API
- Initially pulls down the list of instruments and then subscribes to price updates
- Built in a MVC like pattern with the MarketDataView.tsx being the view and the hook useMarketData.ts being the controller
- Uses a custom built table which includes cell rendering and sorting
- Uses a custom built button

### Improvements (UI)

- Consider using nextjs instead of CRA (bundling, SSR, SSG)
- Consider using AGGrid instead of a custom table for virtualisation & paging
- Consider moving mkt data to an application wide state (to share across the app) e.g. Redux, Recoil, Zustand
- Logging & Metrics (using something like Sentry to push these from the client)
- Testing (automating with something like Cypress)
