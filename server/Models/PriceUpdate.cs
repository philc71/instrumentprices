﻿namespace InstrumentPrices.Api.Models
{
    public class PriceUpdate
    {
        public string Id { get; set; }
        public decimal Price { get; set; }
        public int Direction { get; set; } = 0;
        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
    }
}
