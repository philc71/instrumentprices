﻿using System.Text.Json.Serialization;

namespace InstrumentPrices.Api.Models
{
    public class Instrument
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string Name { get; set; } = string.Empty;

        public decimal Price { get; set; } = 0;

        public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
    }
}
