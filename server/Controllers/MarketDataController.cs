using InstrumentPrices.Api.Models;
using InstrumentPrices.Api.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace InstrumentPrices.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MarketDataController : ControllerBase
    {
        private readonly IInstrumentRepository _instrumentRepository;
        private readonly ILogger<MarketDataController> _logger;

        public MarketDataController(IInstrumentRepository instrumentRepository, ILogger<MarketDataController> logger)
        {
            _instrumentRepository = instrumentRepository;
            _logger = logger;
        }

        [Route("instruments")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Instrument>>> Get()
        {
            var result = await _instrumentRepository.GetInstruments();
            return Ok(result);
        }
    }
}