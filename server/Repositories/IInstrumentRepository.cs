﻿using InstrumentPrices.Api.Models;

namespace InstrumentPrices.Api.Repositories
{
    public interface IInstrumentRepository
    {
        ValueTask<List<Instrument>> GetInstruments();

        ValueTask<List<string>> GetInstrumentIds();

        ValueTask<Instrument?> GetInstrument(string id);

        Task UpdateInstrumentPrice(PriceUpdate update);
    }
}
