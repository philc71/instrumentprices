﻿using InstrumentPrices.Api.Models;
using System;

namespace InstrumentPrices.Api.Repositories
{
    /// <summary>
    /// This repo is intended to own the data loading, caching, updating and persisting it to disk (not implemented) etc
    /// </summary>
    public class InstrumentRepository : IInstrumentRepository
    {
        private List<Instrument> _instruments = new List<Instrument>();

        /// <summary>
        /// Returns instruments from cache (after initial load)
        /// </summary>
        public async ValueTask<List<Instrument>> GetInstruments()
        {
            if (!_instruments.Any())
            {
                await LoadInstruments();
            }
            
            return _instruments;
        }

        public async ValueTask<List<string>> GetInstrumentIds()
        {
            if (!_instruments.Any())
            {
                await LoadInstruments();
            }            

            return _instruments.Select(x => x.Id).ToList();
        }

        public async ValueTask<Instrument?> GetInstrument(string id)
        {
            if (!_instruments.Any())
            {
                await LoadInstruments();
            }

            return _instruments.FirstOrDefault(x => x.Id == id);
        }

        public async Task UpdateInstrumentPrice(PriceUpdate update)
        {
            var instrument = await GetInstrument(update.Id);
            if (instrument != null)
            {
                instrument.Price = update.Price;
                instrument.UpdatedAt = update.UpdatedAt;
            }
        }   

        /// <summary>
        /// Simulates a database initial load of the 10 instruments
        /// </summary>
        private async Task LoadInstruments()
        {
            await Task.Delay(10);

            var result = new List<Instrument>() {
                new Instrument() { Name = "Apple Inc", Price = 150.00M},
                new Instrument() { Name = "Microsoft Corporation", Price = 261.00M},
                new Instrument() { Name = "Amazon.com Inc", Price = 111.99M},
                new Instrument() { Name = "Alphabet Inc", Price = 106.40M},
                new Instrument() { Name = "Berkshire Hathaway Inc", Price = 311.82M},
                new Instrument() { Name = "NVIDIA Corporation", Price = 213.27M},
                new Instrument() { Name = "Tesla Inc", Price = 193.07M},
                new Instrument() { Name = "Exxon Mobil Corporation", Price = 112.19M},
                new Instrument() { Name = "UnitedHealth Group Incorporated", Price = 466.05M},
                new Instrument() { Name = "Johnson & Johnson", Price = 164.06M},
            };

            _instruments = result;
        }
    }
}



