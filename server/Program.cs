using InstrumentPrices.Api.Hubs;
using InstrumentPrices.Api.Repositories;
using InstrumentPrices.Api.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSingleton<IInstrumentRepository, InstrumentRepository>();
builder.Services.AddSingleton<IMarketDataService, MarketDataService>();

builder.Services.AddSignalR();
builder.Services.AddCors(options =>
{
    options.AddPolicy("CorsPolicy",
                   builder =>
                   {
                       builder.WithOrigins("http://localhost:3000")
                                            .AllowAnyHeader()
                                            .AllowAnyMethod()
                                            .AllowCredentials();
                   });
});


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseRouting();

app.UseAuthorization();

app.UseCors("CorsPolicy");

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
    endpoints.MapHub<MarketDataHub>("/marketdata");
});

// spin up the instrument service to load the initial data 
app.Services.GetService<IMarketDataService>();

app.Run();
