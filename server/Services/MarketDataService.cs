﻿using InstrumentPrices.Api.Hubs;
using InstrumentPrices.Api.Models;
using InstrumentPrices.Api.Repositories;
using Microsoft.AspNetCore.SignalR;

namespace InstrumentPrices.Api.Services
{
    public class MarketDataService : IMarketDataService, IDisposable
    {
        private readonly IInstrumentRepository _instrumentRepository;
        private readonly IHubContext<MarketDataHub> _marketDataHub;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private bool _isDisposing = false;
        private int _priceStreamThrottleInMilliseconds = 1000;

        public MarketDataService(IInstrumentRepository instrumentRepository, IHubContext<MarketDataHub> marketDataHub)
        {
            _instrumentRepository = instrumentRepository;
            _marketDataHub = marketDataHub;

            // start price feed in its own thread
            Task.Factory.StartNew(() => Start(),
                _cancellationTokenSource.Token,
                TaskCreationOptions.LongRunning,
                TaskScheduler.Default);
        }

        private async Task Start()
        {
            Random rnd = new Random();

            // get instruments from repository
            var ids = (await _instrumentRepository.GetInstrumentIds()).ToArray();

            while (ids != null && !_cancellationTokenSource.Token.IsCancellationRequested)
            {
                // crude throttling
                await Task.Delay(_priceStreamThrottleInMilliseconds);

                // updates in batches
                var updates = new List<PriceUpdate>();

                // max number of updates in one batch (1/2 the instruments)
                var nbrToUpdate = rnd.Next(5);

                // create updates for a random number of instruments (up to all 10)
                for (int i = 0; i < nbrToUpdate; i++)
                {
                    // grab a random instrument id
                    var idToUpdate = ids[rnd.Next(10)];

                    // ensure we don't create multiple updates for the same instrument
                    if (updates.Exists(x => x.Id == idToUpdate))
                    {
                        continue;
                    }

                    // grab the instrument
                    var instrument = await _instrumentRepository.GetInstrument(idToUpdate);

                    if (instrument != null)
                    {
                        // determine an amount to change the instruments price either up or down
                        decimal change = 1 + (decimal)((rnd.NextDouble() / 100) * (rnd.NextDouble() < 0.5 ? -1 : 1));
                        // calculate the new price given the change
                        decimal newPrice = Math.Round(instrument.Price * change, 2);
                        // form the update message
                        var update = new PriceUpdate()
                        {
                            Id = instrument.Id,
                            Price = newPrice,
                            Direction = instrument.Price == newPrice ? 0 : instrument.Price < newPrice ? 1 : -1,
                        };
                        // update the repo to keep it inline (would be responsible for persistence)
                        await _instrumentRepository.UpdateInstrumentPrice(update);
                        // add to batch
                        updates.Add(update);
                    }
                }

                // send all updates in the batch
                await _marketDataHub.Clients.All.SendAsync("onPriceUpdate", updates);
            }
        }

        public void Dispose()
        {
            if (!_isDisposing)
            {
                _isDisposing = true;
                _cancellationTokenSource.Cancel();
            }
        }
    }
}
