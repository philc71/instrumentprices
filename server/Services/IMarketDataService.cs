﻿namespace InstrumentPrices.Api.Services
{
    // In a real scenario there would be methods to expose but in this simple example there's nothing that needs to call it
    // so this interface is here for DI and mocking purposes for testing
    public interface IMarketDataService
    {
    }
}
