export interface Instrument {
  id: string;
  name: string;
  direction: number;
  price: number;
  updatedAt: string;
}
