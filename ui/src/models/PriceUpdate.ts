export interface PriceUpdate {
  id: string;
  price: number;
  direction: number;
  updatedAt: string;
}
