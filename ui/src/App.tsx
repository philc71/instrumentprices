import React from 'react';
import './App.css';
import { MarketDataView } from './components/MarketDataView';

function App() {
  return (
    <div className="h-screen w-screen bg-zinc-700">
      <MarketDataView />
    </div>
  );
}

export default App;
