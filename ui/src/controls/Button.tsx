export enum ButtonLevel {
  PRIMARY = "PRIMARY",
  SECONDARY = "SECONDARY",
  INFO = "INFO",
  ERROR = "ERROR",
  WARNING = "WARNING",
  SUCCESS = "SUCCESS",
}

export enum ButtonSize {
  SMALL = "SMALL",
  MEDIUM = "MEDIUM",
  LARGE = "LARGE",
}

export interface ButtonProps {
  caption?: string;
  rounded?: boolean;
  level?: ButtonLevel;
  size?: ButtonSize;
  style?: {};
  onClick: () => void;
  disabled?: boolean;
}

const primary =
  "inline-block text-white bg-sky-700 border-0 focus:outline-none hover:bg-sky-600";

const secondary =
  "inline-block text-white bg-gray-600 border-0 focus:outline-none hover:bg-gray-500";

const info =
  "inline-block text-white bg-blue-600 border-0 focus:outline-none hover:bg-blue-500";

const success =
  "inline-block text-white bg-green-600 border-0 focus:outline-none hover:bg-green-500";

const error =
  "inline-block text-white bg-red-600 border-0 focus:outline-none hover:bg-red-500";

const warning =
  "inline-block text-white bg-orange-600 border-0 focus:outline-none hover:bg-orange-500";

const disabled =
  "inline-block text-white bg-gray-400 border-0 focus:outline-none";

export const Button = (props: ButtonProps) => {
  let baseStyle = primary;
  if (props.level && !props.disabled) {
    switch (props.level) {
      case ButtonLevel.SECONDARY:
        baseStyle = secondary;
        break;
      case ButtonLevel.INFO:
        baseStyle = info;
        break;
      case ButtonLevel.SUCCESS:
        baseStyle = success;
        break;
      case ButtonLevel.ERROR:
        baseStyle = error;
        break;
      case ButtonLevel.WARNING:
        baseStyle = warning;
        break;
    }
  } else {
    baseStyle = disabled;
  }

  let baseSize = "text-lg py-2 px-6 h-11";
  if (props.size) {
    switch (props.size) {
      case ButtonSize.SMALL:
        baseSize = "text-sm py-1 px-3 h-8 leading-6";
        break;
      case ButtonSize.LARGE:
        baseSize = "text-xl py-3 px-9 h-15";
        break;
    }
  }

  let combinedStyle = `${baseStyle} ${baseSize}`;
  combinedStyle += props.rounded ? ` rounded-full` : " rounded";

  return (
    <button
      disabled={props.disabled}
      style={props.style ?? {}}
      className={combinedStyle}
      onClick={props.onClick}
    >
      {props.caption ?? ""}
    </button>
  );
};
