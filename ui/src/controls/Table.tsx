import React from 'react';
import { FaArrowDown, FaArrowUp } from 'react-icons/fa';

export interface TableField {
  name: string;
  displayName?: string;
  width?: string;
  cellRenderer?: (data: any) => React.ReactElement | React.ReactElement[];
}

export interface TableProps {
  data: any[];
  fields: TableField[];
}

interface TableData {
  data: any[];
  sortField?: string;
  sortDirection?: string;
}

export const Table = (props: TableProps) => {
  const [tableData, setTableData] = React.useState<TableData>({
    data: props.data,
  });

  const sortData = (data: any[], ascending: boolean, field?: string): any[] => {
    if (field) {
      if (ascending) {
        data.sort((a, b) => {
          if (a[field] < b[field]) {
            return -1;
          }
          if (a[field] > b[field]) {
            return 1;
          }
          return 0;
        });
      } else {
        data.sort((a, b) => {
          if (a[field] < b[field]) {
            return 1;
          }
          if (a[field] > b[field]) {
            return -1;
          }
          return 0;
        });
      }
    }
    return data;
  };

  const sortBy = (field: string) => {
    setTableData((prevState) => {
      let direction = 'asc';
      if (prevState && prevState.sortField === field) {
        direction = prevState.sortDirection === 'asc' ? 'desc' : 'asc';
        return { data: sortData(prevState.data, direction === 'asc', field), sortField: field, sortDirection: direction };
      } else {
        return { data: sortData(prevState.data, true, field), sortField: field, sortDirection: 'asc' };
      }
    });
  };

  React.useEffect(() => {
    setTableData((prevState) => {
      const sortedData = sortData(props.data, prevState.sortDirection === 'asc', prevState.sortField);
      return { ...prevState, data: sortedData };
    });
  }, [props.data]);

  const renderTable = () => {
    return (
      <>
        <table className="table-auto w-full whitespace-no-wrap bg-zinc-800 relative border border-zinc-700">
          <thead>
            <tr className="text-left">
              {props.fields.map((field: TableField) => {
                return (
                  <th
                    key={`${field.name}`}
                    className="bg-zinc-900  border-zinc-700 text-zinc-300 border-t border-b p-2 font-bold tracking-wider uppercase text-xs cursor-pointer"
                    onClick={() => sortBy(field.name)}
                  >
                    <div className="float-left">{field.displayName ?? field.name}</div>
                    {tableData.sortField && tableData.sortField === field.name && tableData.sortDirection === 'asc' && (
                      <div className="float-left ml-1">
                        <FaArrowUp fontSize={11} className="mt-1" />
                      </div>
                    )}
                    {tableData.sortField && tableData.sortField === field.name && tableData.sortDirection === 'desc' && (
                      <div className="float-left ml-1">
                        <FaArrowDown fontSize={11} className="mt-1" />
                      </div>
                    )}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {tableData.data &&
              tableData.data.map((data, idx) => {
                return (
                  <tr className="even:bg-zinc-800 odd:bg-neutral-800" key={`r${idx}`}>
                    {props.fields.map((field: TableField, fidx) => {
                      let style = {};
                      if (field.width) {
                        style = { ...style, width: `${field.width}` };
                      }

                      if (field.cellRenderer) {
                        return (
                          <td key={`rc${idx}:${fidx}`} className="border-solid border-t border-zinc-700" style={style}>
                            {field.cellRenderer(data)}
                          </td>
                        );
                      } else {
                        return (
                          <td key={`rc${idx}:${fidx}`} className="border-solid border-t border-zinc-700" style={style}>
                            <span className="text-zinc-300 px-2 py-1 flex items-center">{data[field.name] ?? ''}</span>
                          </td>
                        );
                      }
                    })}
                  </tr>
                );
              })}
          </tbody>
        </table>
      </>
    );
  };

  const renderEmpty = () => {
    return (
      <div className="h-full w-full bg-zinc-800 border rounded border-zinc-700 grid place-content-center">
        <div className="border rounded border-zinc-700 bg-zinc-800 text-zinc-300 p-5 text-center w-60">No Data</div>
      </div>
    );
  };

  return props?.fields?.length > 0 ? renderTable() : renderEmpty();
};
