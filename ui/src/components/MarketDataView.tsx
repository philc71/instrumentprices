import { HubConnectionState } from '@microsoft/signalr';
import { FaArrowDown, FaArrowUp } from 'react-icons/fa';
import { IoMdCloseCircle, IoMdCheckmarkCircle, IoMdInformationCircle } from 'react-icons/io';
import { Button, ButtonLevel, ButtonSize } from '../controls/Button';
import { Table } from '../controls/Table';
import { Instrument } from '../models/Instrument';
import { useMarketData } from './useMarketData';

export const instrumentFields = [
  {
    name: 'id',
    displayName: 'ID',
    width: '350px',
  },
  {
    name: 'name',
    displayName: 'Name',
    width: '400px',
  },
  {
    name: 'price',
    displayName: 'Price',
    width: '80px',
    cellRenderer: (data: Instrument): React.ReactElement | React.ReactElement[] => {
      if (data.direction !== undefined) {
        switch (data.direction) {
          case 1:
            return (
              <div className="text-green-700 float-right pr-2">
                <div className="inline-block">
                  <FaArrowUp fontSize={12} className="mr-1" />
                </div>
                <div className="inline-block">{data.price.toFixed(2)}</div>
              </div>
            );
          case -1:
            return (
              <div className="text-red-700 float-right pr-2">
                <div className="inline-block">
                  <FaArrowDown fontSize={12} className="mr-1" />
                </div>
                <div className="inline-block">{data.price.toFixed(2)}</div>
              </div>
            );
        }
      }
      return <div className="text-zinc-300 float-right pr-2">{data.price.toFixed(2)}</div>;
    },
  },
  {
    name: 'updatedAt',
    displayName: 'Updated At',
    width: '260px',
  },
];

export const MarketDataView = () => {
  const mktData = useMarketData();

  const isConnected = (): boolean => {
    return mktData.connectionState === HubConnectionState.Connected;
  };

  return (
    <div className="h-full w-full p-2">
      <div className="h-8">
        <div className="float-left w-24">
          <Button size={ButtonSize.SMALL} level={ButtonLevel.PRIMARY} caption="Subscribe" disabled={isConnected()} onClick={mktData.subscribe} />
        </div>
        <div className="float-left w-24">
          <Button size={ButtonSize.SMALL} level={ButtonLevel.PRIMARY} caption="UnSubscribe" disabled={!isConnected()} onClick={mktData.unsubscribe} />
        </div>
      </div>
      <div className="mt-2 float-left w-auto">
        <Table fields={instrumentFields} data={mktData.instruments} />
      </div>
      <div className="clear-both block p-1">
        <div className="float-left">
          {(!mktData.connectionState || mktData.connectionState === HubConnectionState.Disconnected) && (
            <IoMdCloseCircle fontSize={22} className="cursor-pointer text-red-600" />
          )}
          {(mktData.connectionState === HubConnectionState.Connecting || mktData.connectionState === HubConnectionState.Reconnecting) && (
            <IoMdInformationCircle fontSize={22} className="cursor-pointer text-orange-600" />
          )}
          {mktData.connectionState === HubConnectionState.Connected && (
            <IoMdCheckmarkCircle fontSize={22} className="cursor-pointer text-green-600" />
          )}
        </div>
        <div className="float-left ml-1 text-zinc-300 font-semibold">{mktData.connectionState}</div>
      </div>
    </div>
  );
};
