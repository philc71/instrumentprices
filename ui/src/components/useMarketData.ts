import React from 'react';
import axios from 'axios';
import { HubConnectionState, HubConnection, HubConnectionBuilder, LogLevel } from '@microsoft/signalr';
import { Instrument } from './../models/Instrument';
import { PriceUpdate } from './../models/PriceUpdate';

export interface useMarketDataResponse {
  instruments: Instrument[];
  subscribe: () => void;
  unsubscribe: () => void;
  connectionState: HubConnectionState;
}

export const useMarketData = (): useMarketDataResponse => {
  const initialised = React.useRef(false);
  const [instruments, setInstruments] = React.useState<Instrument[]>([]);
  const [connection, setConnection] = React.useState<HubConnection | undefined>(undefined);
  const [connectionState, setConnectionState] = React.useState<HubConnectionState>(HubConnectionState.Disconnected);

  const loadInstruments = async () => {
    try {
      const { data } = await axios.get<Instrument[]>('https://localhost:5400/api/marketdata/instruments', {
        headers: {
          Accept: 'application/json',
        },
      });
      setInstruments(data as Instrument[]);
    } catch (error) {
      console.log(error);
    }
  };

  const subscribe = async () => {
    if (connection) {
      await connection.start();
      setConnectionState(connection.state);
    }
  };

  const unsubscribe = async () => {
    if (connection) {
      await connection.stop();
      setConnectionState(connection.state);
    }
  };

  const createConnection = async () => {
    try {
      const conn: HubConnection = new HubConnectionBuilder()
        .withUrl(`https://localhost:5400/marketdata`)
        .configureLogging(LogLevel.Information)
        .build();
      conn.serverTimeoutInMilliseconds = 15000;
      conn.keepAliveIntervalInMilliseconds = 7500;
      conn.onclose(() => {
        setConnectionState(HubConnectionState.Disconnected);
      });
      conn.onreconnecting(() => {
        setConnectionState(HubConnectionState.Reconnecting);
      });
      conn.onreconnected(() => {
        setConnectionState(HubConnectionState.Connected);
      });
      conn.on('onPriceUpdate', (updates: PriceUpdate[]) => {
        if (updates && updates.length) {
          setInstruments((prevState) => {
            const result = [...prevState];
            updates.forEach((update) => {
              const instrument = result.find((x) => x.id === update.id);
              if (instrument) {
                instrument.direction = update.direction;
                instrument.price = update.price;
                instrument.updatedAt = update.updatedAt;
              }
            });
            return result;
          });
        }
      });
      setConnection(conn);
    } catch (error) {
      console.log(error);
    }
  };

  React.useEffect(() => {
    // using ref due to this change (which is not an issue in a production build)
    // https://upmostly.com/tutorials/why-is-my-useeffect-hook-running-twice-in-react
    if (!initialised.current) {
      initialised.current = true;
      loadInstruments();
      createConnection();
    }
  }, []);

  return {
    instruments,
    subscribe,
    unsubscribe,
    connectionState,
  };
};
